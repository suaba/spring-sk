package com.capgemini.chess.service.access.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.capgemini.chess.dataaccess.entities.UserEntity;
import com.capgemini.chess.service.access.UserDAO;
import com.capgemini.chess.service.mapper.UserProfileMapper;
import com.capgemini.chess.service.to.UserProfileTO;

@Repository
public class MapUserDAO implements UserDAO {
	
	private final List<UserEntity> users = new ArrayList<>(); //XXX

	@Override
	public UserProfileTO update(UserProfileTO to) {
		
		UserEntity user = UserProfileMapper.map(to);
		return UserProfileMapper.map(user);
	}

	@Override
	public UserProfileTO findByEmail(String email) {
		UserEntity user = users.stream().filter(u -> u.getEmail().equals(email)).findFirst().orElse(null);
		return UserProfileMapper.map(user);
	}

}
