package com.capgemini.chess.service.mapper;

import com.capgemini.chess.dataaccess.entities.MatchEntity;
import com.capgemini.chess.service.to.MatchTO;

public class MatchMapper {
	
	public static MatchTO map(MatchEntity matchEntity) {
		if (matchEntity != null) {
			MatchTO matchTO = new MatchTO();
			matchTO.setGuest(UserProfileMapper.map(matchEntity.getGuest()));
			matchTO.setHost(UserProfileMapper.map(matchEntity.getHost()));
			matchTO.setId(matchEntity.getId());
			matchTO.setResult(matchEntity.getResult());
			return matchTO;
		}
		return null;
	}
	
	public static MatchEntity map(MatchTO matchTO) {
		if (matchTO != null) {
			MatchEntity matchEntity = new MatchEntity();
			matchEntity.setGuest(UserProfileMapper.map(matchTO.getGuest()));
			matchEntity.setHost(UserProfileMapper.map(matchTO.getHost()));
			matchEntity.setId(matchTO.getId());
			matchEntity.setResult(matchTO.getResult());
			return matchEntity;
		}
		return null;
	}
}

