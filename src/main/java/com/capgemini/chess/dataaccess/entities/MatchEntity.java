package com.capgemini.chess.dataaccess.entities;

import com.capgemini.chess.enums.MatchStatus;

public class MatchEntity {
	private Integer id;
	private UserEntity host;
	private UserEntity guest;
	private MatchStatus result;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public UserEntity getHost() {
		return host;
	}
	public void setHost(UserEntity host) {
		this.host = host;
	}
	public UserEntity getGuest() {
		return guest;
	}
	public void setGuest(UserEntity guest) {
		this.guest = guest;
	}
	public MatchStatus getResult() {
		return result;
	}
	public void setResult(MatchStatus result) {
		this.result = result;
	}
	
}
