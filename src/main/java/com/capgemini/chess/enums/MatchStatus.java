package com.capgemini.chess.enums;

public enum MatchStatus {
	WON,
	DRAWN,
	LOSE
}
