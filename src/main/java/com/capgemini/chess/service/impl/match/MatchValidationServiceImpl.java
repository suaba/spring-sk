package com.capgemini.chess.service.impl.match;

import org.springframework.stereotype.Service;

import com.capgemini.chess.exception.MatchValidationException;
import com.capgemini.chess.service.MatchValidationService;
import com.capgemini.chess.service.to.MatchTO;

@Service
public class MatchValidationServiceImpl implements MatchValidationService {
	
	@Override
	public MatchTO validate(MatchTO to) throws MatchValidationException {
		if (to.getGuest() == null || to.getHost() == null) {
			throw new MatchValidationException("Match does not contain host and guest");
		}
		if (to.getGuest().getEmail() == to.getHost().getEmail()) {
			throw new MatchValidationException("Host and guest cannot be the same");
		}
		return to;
	}

}
