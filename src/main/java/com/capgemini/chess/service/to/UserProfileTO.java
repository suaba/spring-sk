package com.capgemini.chess.service.to;

import com.capgemini.chess.enums.MatchStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UserProfileTO implements Comparable<UserProfileTO>{
	
	private long id;
	private String login;
	private String password;
	private String name;
	private String surname;
	private String email;
	private String aboutMe;
	private String lifeMotto;
	private UserStatisticsTO statistics;
	
	public UserProfileTO() {
		
	}
	
	public UserProfileTO(String login) {
		this.login = login;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	@JsonIgnore
	public String getPassword() {
		return password;
	}
	
	@JsonProperty
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getSurname() {
		return surname;
	}
	
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getAboutMe() {
		return aboutMe;
	}
	
	public void setAboutMe(String aboutMe) {
		this.aboutMe = aboutMe;
	}
	
	public String getLifeMotto() {
		return lifeMotto;
	}
	
	public void setLifeMotto(String lifeMotto) {
		this.lifeMotto = lifeMotto;
	}

	public UserStatisticsTO getStatistics() {
		return statistics;
	}

	public void setStatistics(UserStatisticsTO statistics) {
		this.statistics = statistics;
	}
	
	public void updatePoints(int points) {
		UserStatisticsTO stats = this.getStatistics();
		int currentPoints = stats.getPosition();
		currentPoints += points;
		stats.setPoints(currentPoints);
		this.setStatistics(stats);
	}
	
	public void updateGames(MatchStatus status) {
		if (status == null) {
			return;
		}
		
		UserStatisticsTO stats = this.getStatistics();
		int gamesPlayed = stats.getGamesPlayed();
		int gamesWon = stats.getGamesWon();
		int gamesLost = stats.getGamesLost();
		int gamesDrawn = stats.getGamesDrawn();
		
		gamesPlayed++;
		stats.setGamesPlayed(gamesPlayed);
		
		switch (status) {
			case WON:
				gamesWon++;
				stats.setGamesWon(gamesWon);
				break;
			case LOSE:
				gamesLost++;
				stats.setGamesLost(gamesLost);
				break;
			case DRAWN:
				gamesDrawn++;
				stats.setGamesDrawn(gamesDrawn);
				break;
		}
		this.setStatistics(stats);
	}
	public void updateLevel() {
		UserStatisticsTO stats = this.getStatistics();
		int points = stats.getPoints();
		int level = stats.getLevel();
		if (points < 50) {
			level = 1;
		} else if (points >= 50 && points < 100) {
			level = 2;
		} else if (points >= 100) {
			level = 3;
		}
		stats.setLevel(level);
		this.setStatistics(stats);
	}

	@Override
	public int compareTo(UserProfileTO o) {
		int comparedPoints = o.getStatistics().getPoints();
		
		if (comparedPoints == 0) {
			return login.compareTo(o.login);
		} else {
			return comparedPoints;
		}
	}
	
}
