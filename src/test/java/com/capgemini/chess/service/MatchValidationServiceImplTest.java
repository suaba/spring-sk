package com.capgemini.chess.service;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;

import com.capgemini.chess.enums.MatchStatus;
import com.capgemini.chess.exception.MatchValidationException;
import com.capgemini.chess.exception.UserValidationException;
import com.capgemini.chess.service.impl.match.MatchValidationServiceImpl;
import com.capgemini.chess.service.to.MatchTO;
import com.capgemini.chess.service.to.UserProfileTO;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration
public class MatchValidationServiceImplTest {
	
	private MatchValidationService matchValidationService = null;
	
	@Before
	public void setup() throws UserValidationException {
		matchValidationService = new MatchValidationServiceImpl();
	}
	
	@Test
	public void shouldThrowWhenNoHostGiven() {
		// given
		MatchTO match = createMatchWithOnlyGuest();
		boolean isExceptionThrown = false;
		// when
		try {
			matchValidationService.validate(match);
		} catch (MatchValidationException e) {
			isExceptionThrown = true;
		}
		// then
		assertTrue(isExceptionThrown);
	}
	
	@Test
	public void shouldThrowWhenNoGuestGiven() {
		// given
		MatchTO match = createMatchWithOnlyHost();
		boolean isExceptionThrown = false;
		// when
		try {
			matchValidationService.validate(match);
		} catch (MatchValidationException e) {
			isExceptionThrown = true;
		}
		// then
		assertTrue(isExceptionThrown);
	}
	
	@Test
	public void shouldThrowWhenHostAndGuestAreTheSame() {
		// given
		MatchTO match = createMatchTheSameHostGuest();
		boolean isExceptionThrown = false;
		// when
		try {
			matchValidationService.validate(match);
		} catch (MatchValidationException e) {
			isExceptionThrown = true;
		}
		// then
		assertTrue(isExceptionThrown);
	}
	
	@Test
	public void shouldNotThrowWhenMatchIsValid() {
		// given
		MatchTO match = createValidMatch();
		boolean isExceptionThrown = false;
		// when
		try {
			matchValidationService.validate(match);
		} catch (MatchValidationException e) {
			isExceptionThrown = true;
		}
		// then
		assertTrue(!isExceptionThrown);
	}
	
	private MatchTO createValidMatch() {
		UserProfileTO host = createTestUser1();
		UserProfileTO guest = createTestUser2(); 
		
		MatchStatus result = MatchStatus.WON;
		MatchTO match = new MatchTO();
		match.setGuest(guest);
		match.setHost(host);
		match.setResult(result);
		return match;
	}
	
	private MatchTO createMatchWithOnlyHost() {
		UserProfileTO host = createTestUser1();
		UserProfileTO guest = null; 
		
		MatchStatus result = MatchStatus.WON;
		MatchTO match = new MatchTO();
		match.setGuest(guest);
		match.setHost(host);
		match.setResult(result);
		return match;
	}
	
	private MatchTO createMatchWithOnlyGuest() {
		UserProfileTO host = null;
		UserProfileTO guest = createTestUser2(); 
		
		MatchStatus result = MatchStatus.WON;
		MatchTO match = new MatchTO();
		match.setGuest(guest);
		match.setHost(host);
		match.setResult(result);
		return match;
	}
	
	private MatchTO createMatchTheSameHostGuest() {
		UserProfileTO host = createTestUser1();
		UserProfileTO guest = createTestUser1(); 
		
		MatchStatus result = MatchStatus.WON;
		MatchTO match = new MatchTO();
		match.setGuest(guest);
		match.setHost(host);
		match.setResult(result);
		return match;
	}
	
	private UserProfileTO createTestUser1() {
		UserProfileTO user = new UserProfileTO();
		user.setEmail("steve@bar.com");
		user.setId(1);
		user.setLifeMotto("Do what you had to");
		user.setLogin("stallone");
		user.setName("Sylvester");
		user.setSurname("Stallone");
		user.setPassword("steve123");
		user.setAboutMe("Human being");
		user.setStatistics(null);
		return user;
	}
	
	private UserProfileTO createTestUser2() {
		UserProfileTO user = new UserProfileTO();
		user.setAboutMe("yoopikay yeey");
		user.setEmail("john@bar.com");
		user.setId(2);
		user.setLifeMotto("Human being");
		user.setLogin("mcclane");
		user.setName("John");
		user.setPassword("A");
		user.setStatistics(null);
		user.setSurname("McClane");
		return user;
	}
}
