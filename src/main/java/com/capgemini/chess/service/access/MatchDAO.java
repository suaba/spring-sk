package com.capgemini.chess.service.access;

import com.capgemini.chess.service.to.MatchTO;

public interface MatchDAO {
	
	public MatchTO getById(int id);
	
	public MatchTO save(MatchTO to);
}
