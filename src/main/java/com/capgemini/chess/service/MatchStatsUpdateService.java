package com.capgemini.chess.service;

import com.capgemini.chess.service.to.MatchTO;

public interface MatchStatsUpdateService {

	public MatchTO update(MatchTO matchTo);
}
