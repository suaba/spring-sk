package com.capgemini.chess.service.to;

import com.capgemini.chess.enums.MatchStatus;

public class MatchTO {
	private int id;
	private UserProfileTO host;
	private UserProfileTO guest;
	private MatchStatus result;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public UserProfileTO getHost() {
		return host;
	}
	public void setHost(UserProfileTO host) {
		this.host = host;
	}
	public UserProfileTO getGuest() {
		return guest;
	}
	public void setGuest(UserProfileTO guest) {
		this.guest = guest;
	}
	public MatchStatus getResult() {
		return result;
	}
	public void setResult(MatchStatus result) {
		this.result = result;
	}
	
}
