package com.capgemini.chess.service;

import com.capgemini.chess.service.to.RankingTO;

public interface RankingReadService {
	public RankingTO getRanking(long id);
}
