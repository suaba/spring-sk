package com.capgemini.chess.service.impl.user;

import org.springframework.beans.factory.annotation.Autowired;

import com.capgemini.chess.exception.UserValidationException;
import com.capgemini.chess.service.UserValidationService;
import com.capgemini.chess.service.access.UserDAO;
import com.capgemini.chess.service.to.UserProfileTO;


public class UserValidationServiceImpl implements UserValidationService {
	
	@Autowired
	private UserDAO userDao;

	@Override
	public void validateUser(UserProfileTO to) throws UserValidationException {
		validateEmail(to);
		validatePassword(to);
		validateExistence(to);
	}

	private void validateEmail(UserProfileTO to) throws UserValidationException {
		if (to.getEmail() == null || to.getEmail() == "") {
			throw new UserValidationException("E-mail not provided");
		}
	}

	private void validatePassword(UserProfileTO to) throws UserValidationException {
		if (to.getPassword() == null || to.getPassword() == "") {
			throw new UserValidationException("Password not provided");
		}
	}
	
	private void validateExistence(UserProfileTO to) throws UserValidationException {
		String email = to.getEmail();
		UserProfileTO foundByEmail = userDao.findByEmail(email);
		if (foundByEmail == null) {
			throw new UserValidationException("User does not exist");
		}
	}

}
