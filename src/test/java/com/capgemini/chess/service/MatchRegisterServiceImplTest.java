package com.capgemini.chess.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;

import com.capgemini.chess.enums.MatchStatus;
import com.capgemini.chess.exception.MatchValidationException;
import com.capgemini.chess.exception.UserValidationException;
import com.capgemini.chess.service.access.MatchDAO;
import com.capgemini.chess.service.access.UserDAO;
import com.capgemini.chess.service.to.MatchTO;
import com.capgemini.chess.service.to.UserProfileTO;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration
public class MatchRegisterServiceImplTest {
	
	@Mock
	private MatchDAO matchDAO;
	@Mock
	private MatchValidationService matchValidationService;
	@Mock
	private MatchStatsUpdateService matchStatsUpdateService;
	
	@Before
	public void setup() throws MatchValidationException {
		given(matchDAO.save(Mockito.any(MatchTO.class))).willReturn(createDataToRegister());
		given(matchValidationService.validate(Mockito.any(MatchTO.class))).willReturn(createDataToRegister());
		given(matchStatsUpdateService.update(Mockito.any(MatchTO.class))).willReturn(createDataToRegister());
	}
	
	@Test
	public void shouldCheckThatMatchCanBeRegistered() {
		// given
		MatchTO newMatch = createDataToRegister();
		
		// when
		try {
			matchValidationService.validate(newMatch);
		} catch (MatchValidationException e) {}
		matchStatsUpdateService.update(newMatch);
		MatchTO result = matchDAO.save(newMatch);
		
		// then
		assertNotNull(result);
		assertEquals(newMatch.getId(), result.getId());
		assertEquals(newMatch.getGuest().getId(), result.getGuest().getId());
		assertEquals(newMatch.getHost().getId(), result.getHost().getId());
		assertEquals(newMatch.getResult(), result.getResult());
	}
	
	@Test
	public void shouldCheckThatUserStatisticCanBeUpdated() {
		// given
		MatchTO newMatch = createDataToRegister();
		
		// when
		try {
			matchValidationService.validate(newMatch);
		} catch (MatchValidationException e) {}
		matchStatsUpdateService.update(newMatch);
		MatchTO result = matchDAO.save(newMatch);
		
		// then
		assertNotNull(result);
		assertEquals(newMatch.getId(), result.getId());
		assertEquals(newMatch.getGuest().getId(), result.getGuest().getId());
		assertEquals(newMatch.getHost().getId(), result.getHost().getId());
		assertEquals(newMatch.getResult(), result.getResult());
	}
	
	private MatchTO createDataToRegister() {
		UserProfileTO host = new UserProfileTO();
		host.setEmail("steve@bar.com");
		host.setId(1);
		host.setLifeMotto("Do what you had to");
		host.setLogin("stallone");
		host.setName("Sylvester");
		host.setSurname("Stallone");
		host.setPassword("steve123");
		host.setAboutMe("Human being");
		host.setStatistics(null);

		
		UserProfileTO guest = new UserProfileTO();
		host.setAboutMe("yoopikay yeey");
		host.setEmail("john@bar.com");
		host.setId(1);
		host.setLifeMotto("Human being");
		host.setLogin("mcclane");
		host.setName("John");
		host.setPassword("A");
		host.setStatistics(null);
		host.setSurname("McClane");
		
		MatchStatus result = MatchStatus.WON;
		
		MatchTO match = new MatchTO();
		match.setGuest(guest);
		match.setHost(host);
		match.setResult(result);
		return match;
	}

}
