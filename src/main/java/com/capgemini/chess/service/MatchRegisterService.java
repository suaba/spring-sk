package com.capgemini.chess.service;

import com.capgemini.chess.exception.MatchValidationException;
import com.capgemini.chess.service.to.MatchTO;

public interface MatchRegisterService {
	public MatchTO registerNewMatch(MatchTO to) throws MatchValidationException;
}
