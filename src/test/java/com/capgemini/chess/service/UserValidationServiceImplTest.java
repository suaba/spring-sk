package com.capgemini.chess.service;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.when;

import com.capgemini.chess.exception.UserValidationException;
import com.capgemini.chess.service.access.UserDAO;
import com.capgemini.chess.service.impl.user.UserValidationServiceImpl;
import com.capgemini.chess.service.to.UserProfileTO;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration
public class UserValidationServiceImplTest {
	
	@InjectMocks
	private UserValidationService userValidationService = new UserValidationServiceImpl();
	
	@Mock
	private UserDAO userDAO;

	@Before
	public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }
	
	@Before
	public void setup() throws UserValidationException {
		userValidationService = new UserValidationServiceImpl();
	}
	
	@Test
	public void shouldThrowWhenNoPasswordGiven() {
		// given
		UserProfileTO user = createUserWithoutPassword();
		boolean isExceptionThrown = false;
		// when
		try {
			userValidationService.validateUser(user);
		} catch (UserValidationException e) {
			isExceptionThrown = true;
		}
		// then
		assertTrue(isExceptionThrown);
	}
	
	@Test
	public void shouldThrowWhenNoEmailGiven() {
		// given
		UserProfileTO user = createUserWithoutEmail();
		boolean isExceptionThrown = false;
		// when
		try {
			userValidationService.validateUser(user);
		} catch (UserValidationException e) {
			isExceptionThrown = true;
		}
		// then
		assertTrue(isExceptionThrown);
	}

	@Test
	public void shouldThrowWhenUserDoesNotExist() {
		// given
		when(userDAO.findByEmail("no@no.com")).thenReturn(null);
		UserProfileTO user = createNotExistingUser();
		boolean isExceptionThrown = false;
		// when
		try {
			userValidationService.validateUser(user);
		} catch (UserValidationException e) {
			isExceptionThrown = true;
		}
		// then
		assertTrue(isExceptionThrown);
	}
	
	private UserProfileTO createUserWithoutPassword() {
		UserProfileTO user = new UserProfileTO();
		user.setAboutMe("A");
		user.setEmail("foo@bar.com");
		user.setId(1);
		user.setLifeMotto("A");
		user.setLogin("jamesbond");
		user.setName("John");
		user.setPassword("");
		user.setStatistics(null);
		user.setSurname("McClane");
		return user;
	}
	
	private UserProfileTO createUserWithoutEmail() {
		UserProfileTO user = new UserProfileTO();
		user.setAboutMe("A");
		user.setEmail("");
		user.setId(1);
		user.setLifeMotto("A");
		user.setLogin("jamesbond");
		user.setName("John");
		user.setPassword("");
		user.setStatistics(null);
		user.setSurname("McClane");
		return user;
	}

	private UserProfileTO createNotExistingUser() {
		UserProfileTO user = new UserProfileTO();
		user.setAboutMe("A");
		user.setEmail("no@no.com");
		user.setId(1);
		user.setLifeMotto("A");
		user.setLogin("jamesbond");
		user.setName("John");
		user.setPassword("ugauga");
		user.setStatistics(null);
		user.setSurname("McClane");
		return user;
	}
	
	private UserProfileTO createUser() {
		UserProfileTO user = new UserProfileTO();
		user.setAboutMe("A");
		user.setEmail("");
		user.setId(1);
		user.setLifeMotto("A");
		user.setLogin("jamesbond");
		user.setName("John");
		user.setPassword("ugauga");
		user.setStatistics(null);
		user.setSurname("McClane");
		return user;
	}
}
