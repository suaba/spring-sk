package com.capgemini.chess.service.access.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.capgemini.chess.service.access.RankingDAO;
import com.capgemini.chess.service.to.UserProfileTO;

@Repository
public class MapRankingDAO implements RankingDAO {
	
	private final List<UserProfileTO> ranking = new ArrayList<>();
	
	@Override
	public int getPosition(long id) throws Exception {
		for (int i = 0; i < ranking.size(); i++) {
			if (ranking.get(i).getId() == id) {
				return i;
			} 
		}
		throw new Exception("User does not exist in ranking");
	}

	@Override
	public ArrayList<UserProfileTO> sort(ArrayList<UserProfileTO> users) {
		Collections.sort(users);
		return users;
	}

}
