package com.capgemini.chess.service.access;

import com.capgemini.chess.service.to.UserProfileTO;

public interface UserDAO {
	
	UserProfileTO update(UserProfileTO entity);

	UserProfileTO findByEmail(String email);
	
}
