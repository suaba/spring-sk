package com.capgemini.chess.service.impl.user;

import org.springframework.beans.factory.annotation.Autowired;

import com.capgemini.chess.exception.UserValidationException;
import com.capgemini.chess.service.UserUpdateService;
import com.capgemini.chess.service.UserValidationService;
import com.capgemini.chess.service.access.UserDAO;
import com.capgemini.chess.service.to.UserProfileTO;

public class UserUpdateServiceImpl implements UserUpdateService {

	@Autowired
	private UserDAO userDAO;
	
	@Autowired
	private UserValidationService userValidationService;
	
	@Override
	public UserProfileTO updateUser(UserProfileTO user) throws UserValidationException {
		userValidationService.validateUser(user);
		return userDAO.update(user);
	}

}
