package com.capgemini.chess.service.mapper;

import com.capgemini.chess.dataaccess.entities.UserStatisticsEntity;
import com.capgemini.chess.service.to.UserStatisticsTO;

public class UserStatisticsMapper {
	
	public static UserStatisticsTO map(UserStatisticsEntity userStatsEntity) {
		if (userStatsEntity != null) {
			UserStatisticsTO userStatsTO = new UserStatisticsTO();
			userStatsTO.setGamesDrawn(userStatsEntity.getGamesDrawn());
			userStatsTO.setGamesLost(userStatsEntity.getGamesLost());
			userStatsTO.setGamesPlayed(userStatsEntity.getGamesPlayed());
			userStatsTO.setGamesWon(userStatsEntity.getGamesWon());
			userStatsTO.setId(userStatsEntity.getId());
			userStatsTO.setLevel(userStatsEntity.getLevel());
			userStatsTO.setPoints(userStatsEntity.getPoints());
			userStatsTO.setPosition(userStatsEntity.getPosition());
			return userStatsTO;
		}
		return null;
	}
	
	public static UserStatisticsEntity map(UserStatisticsTO userStatsTO) {
		if (userStatsTO != null) {
			UserStatisticsEntity userStatsEntity = new UserStatisticsEntity();
			userStatsEntity.setGamesDrawn(userStatsTO.getGamesDrawn());
			userStatsEntity.setGamesLost(userStatsTO.getGamesLost());
			userStatsEntity.setGamesPlayed(userStatsTO.getGamesPlayed());
			userStatsEntity.setGamesWon(userStatsTO.getGamesWon());
			userStatsEntity.setId(userStatsTO.getId());
			userStatsEntity.setLevel(userStatsTO.getLevel());
			userStatsEntity.setPoints(userStatsTO.getPoints());
			userStatsEntity.setPosition(userStatsTO.getPosition());
			return userStatsEntity;
		}
		return null;
	}
	
	
}
