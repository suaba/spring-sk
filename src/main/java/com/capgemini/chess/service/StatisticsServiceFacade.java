package com.capgemini.chess.service;

import com.capgemini.chess.exception.MatchValidationException;
import com.capgemini.chess.exception.UserValidationException;
import com.capgemini.chess.service.to.MatchTO;
import com.capgemini.chess.service.to.RankingTO;
import com.capgemini.chess.service.to.UserProfileTO;

public interface StatisticsServiceFacade {
	RankingTO getRanking(long id);
	
	MatchTO registerNewMatch(MatchTO to) throws MatchValidationException;
	
	UserProfileTO updateUser(UserProfileTO user) throws UserValidationException;
}
