package com.capgemini.chess.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.capgemini.chess.exception.MatchValidationException;
import com.capgemini.chess.exception.UserValidationException;
import com.capgemini.chess.service.MatchRegisterService;
import com.capgemini.chess.service.RankingReadService;
import com.capgemini.chess.service.StatisticsServiceFacade;
import com.capgemini.chess.service.UserUpdateService;
import com.capgemini.chess.service.to.MatchTO;
import com.capgemini.chess.service.to.RankingTO;
import com.capgemini.chess.service.to.UserProfileTO;

public class StatisticsServiceFacadeImpl implements StatisticsServiceFacade {
	
	@Autowired
	RankingReadService rankingReadService;
	@Autowired
	MatchRegisterService matchRegisterService;
	@Autowired
	UserUpdateService userUpdateService;	

	@Override
	public RankingTO getRanking(long id) {
		return rankingReadService.getRanking(id);
	}

	@Override
	public MatchTO registerNewMatch(MatchTO to) throws MatchValidationException {
		return matchRegisterService.registerNewMatch(to);
	}

	@Override
	public UserProfileTO updateUser(UserProfileTO user) throws UserValidationException {
		return userUpdateService.updateUser(user);
	}

}
