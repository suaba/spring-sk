package com.capgemini.chess.service.impl.match;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.chess.enums.MatchStatus;
import com.capgemini.chess.service.MatchStatsUpdateService;
import com.capgemini.chess.service.access.UserDAO;
import com.capgemini.chess.service.to.MatchTO;
import com.capgemini.chess.service.to.UserProfileTO;

@Service
public class MatchStatsUpdateServiceImpl implements MatchStatsUpdateService {
	
	@Autowired
	private UserDAO user;
	@Autowired
	private UserProfileTO host;
	@Autowired
	private UserProfileTO guest;
	
	@Override
	public MatchTO update(MatchTO matchTo) {
		MatchStatus result = matchTo.getResult();
		host = matchTo.getHost();
		guest = matchTo.getGuest();
		switch (result) {
			case WON: 
				host.updateGames(result);
				host.updatePoints(10);
				host.updateLevel();
			
				guest.updateGames(MatchStatus.LOSE);
				guest.updatePoints(-10);
				guest.updateLevel();
				break;
			case DRAWN:
				host.updateGames(result);
				guest.updateGames(result);
				break;
			case LOSE:
				host.updateGames(result);
				host.updatePoints(-10);
				host.updateLevel();
			
				guest.updateGames(MatchStatus.WON);
				guest.updatePoints(10);
				guest.updateLevel();
				break;
		}
		user.update(guest);
		user.update(host);
		return matchTo;
	}
}
