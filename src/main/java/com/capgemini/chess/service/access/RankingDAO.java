package com.capgemini.chess.service.access;

import java.util.ArrayList;

import com.capgemini.chess.service.to.UserProfileTO;

public interface RankingDAO {

	public int getPosition(long id) throws Exception;
	
	public ArrayList<UserProfileTO> sort(ArrayList<UserProfileTO> unsorted);
}
