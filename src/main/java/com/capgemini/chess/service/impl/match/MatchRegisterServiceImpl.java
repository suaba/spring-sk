package com.capgemini.chess.service.impl.match;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.chess.exception.MatchValidationException;
import com.capgemini.chess.service.MatchRegisterService;
import com.capgemini.chess.service.MatchValidationService;
import com.capgemini.chess.service.access.MatchDAO;
import com.capgemini.chess.service.to.MatchTO;

@Service
public class MatchRegisterServiceImpl implements MatchRegisterService {

	@Autowired
	private MatchDAO matchDAO;
	@Autowired
	private MatchValidationService matchValidationService;
	@Autowired
	private MatchStatsUpdateServiceImpl matchStatsUpdateService;
	
	@Override
	public MatchTO registerNewMatch(MatchTO to) throws MatchValidationException {
		matchValidationService.validate(to);
		matchStatsUpdateService.update(to);
		return matchDAO.save(to);
	}
}
