package com.capgemini.chess.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;

import com.capgemini.chess.exception.UserValidationException;
import com.capgemini.chess.service.access.UserDAO;
import com.capgemini.chess.service.to.UserProfileTO;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration
public class UserUpdateServiceImplTest {
	
	@Mock
	private UserDAO userDAO;
	
	@Mock
	private UserUpdateService userUpdateService;
	
	@Before
	public void setup() throws UserValidationException {
		given(userDAO.findByEmail(Mockito.any(String.class))).willReturn(createStoredUser());
		given(userDAO.update(Mockito.any(UserProfileTO.class))).willReturn(createDataToUpdate());
		given(userUpdateService.updateUser(Mockito.any(UserProfileTO.class))).willReturn(createDataToUpdate());
	}
	
	@Test
	public void shouldCheckThatUserCanBeUpdated() {
		// given
		UserProfileTO newUser = createDataToUpdate();
		
		// when
		UserProfileTO storedUser = userDAO.findByEmail(newUser.getEmail());
		try {
			storedUser = userUpdateService.updateUser(newUser);	
		} catch (UserValidationException e) {}
		// then
		assertNotNull(storedUser);
		assertEquals(newUser.getName(), storedUser.getName());
		assertEquals(newUser.getSurname(), storedUser.getSurname());
	}

	private UserProfileTO createDataToUpdate() {
		UserProfileTO user = new UserProfileTO();
		user.setAboutMe("A");
		user.setEmail("foo@bar.com");
		user.setId(1);
		user.setLifeMotto("A");
		user.setLogin("jamesbond");
		user.setName("James");
		user.setPassword("A");
		user.setStatistics(null);
		user.setSurname("Bond");
		return user;
	}
	
	private UserProfileTO createStoredUser() {
		UserProfileTO user = new UserProfileTO();
		user.setAboutMe("A");
		user.setEmail("foo@bar.com");
		user.setId(1);
		user.setLifeMotto("A");
		user.setLogin("jamesbond");
		user.setName("John");
		user.setPassword("A");
		user.setStatistics(null);
		user.setSurname("McClane");
		return user;
	}

}
