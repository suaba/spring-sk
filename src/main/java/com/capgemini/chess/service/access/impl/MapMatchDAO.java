package com.capgemini.chess.service.access.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.capgemini.chess.dataaccess.entities.MatchEntity;
import com.capgemini.chess.service.access.MatchDAO;
import com.capgemini.chess.service.mapper.MatchMapper;
import com.capgemini.chess.service.to.MatchTO;

@Repository
public class MapMatchDAO implements MatchDAO {
	
	private final List<MatchEntity> matches = new ArrayList<>();

	@Override
	public MatchTO getById(int id) {
		
		MatchEntity match = matches.stream().filter(m -> m.getId().equals(id)).findFirst().orElse(null);
		return MatchMapper.map(match);
	}

	@Override
	public MatchTO save(MatchTO to) {
		MatchEntity match = MatchMapper.map(to);
		matches.add(match);
		return MatchMapper.map(match);
	}

}
