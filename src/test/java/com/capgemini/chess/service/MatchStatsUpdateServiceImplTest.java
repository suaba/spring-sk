package com.capgemini.chess.service;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.capgemini.chess.enums.MatchStatus;
import com.capgemini.chess.exception.MatchValidationException;
import com.capgemini.chess.exception.UserValidationException;
import com.capgemini.chess.service.access.UserDAO;
import com.capgemini.chess.service.impl.match.MatchStatsUpdateServiceImpl;
import com.capgemini.chess.service.impl.match.MatchValidationServiceImpl;
import com.capgemini.chess.service.to.MatchTO;
import com.capgemini.chess.service.to.UserProfileTO;
import com.capgemini.chess.service.to.UserStatisticsTO;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration
public class MatchStatsUpdateServiceImplTest {
	
	@Mock
	private UserDAO userDAO;
	@Mock
	private UserProfileTO host;
	@Mock
	private UserProfileTO guest;
	
	@InjectMocks
	private MatchStatsUpdateService matchStatsUpdateService = new MatchStatsUpdateServiceImpl();
	
	@Before
	public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }
	
	@Before
	public void setup() throws UserValidationException {
		matchStatsUpdateService = new MatchStatsUpdateServiceImpl();
		
	}
	
	@Test
	public void shouldUpdatePointsWhenHostWins() {
		// given
		given(userDAO.findByEmail(Mockito.any(String.class))).willReturn(createTestUser1());
		MatchTO match = createHostWonMatch();

		// when
		MatchTO result = matchStatsUpdateService.update(match);
		// then
		assertEquals(10, result.getHost().getStatistics().getPoints());
	}
	
	@Test
	public void shouldProperUpdateWhenHostWins() {
		// given
		given(userDAO.findByEmail(Mockito.any(String.class))).willReturn(createTestUser1());
		MatchTO match = createHostWonMatch();

		// when
		MatchTO result = matchStatsUpdateService.update(match);
		// then
		assertEquals(1, result.getHost().getStatistics().getGamesPlayed());
		assertEquals(1, result.getHost().getStatistics().getGamesWon());
		assertEquals(0, result.getHost().getStatistics().getGamesLost());
		
		assertEquals(1, result.getGuest().getStatistics().getGamesPlayed());
		assertEquals(0, result.getGuest().getStatistics().getGamesWon());
		assertEquals(1, result.getGuest().getStatistics().getGamesLost());
	}
	
	@Test
	public void shouldProperUpdateWhenGuestWins() {
		// given
		given(userDAO.findByEmail(Mockito.any(String.class))).willReturn(createTestUser1());
		MatchTO match = createGuestWonMatch();

		// when
		MatchTO result = matchStatsUpdateService.update(match);
		// then
		assertEquals(1, result.getHost().getStatistics().getGamesPlayed());
		assertEquals(0, result.getHost().getStatistics().getGamesWon());
		assertEquals(1, result.getHost().getStatistics().getGamesLost());
		
		assertEquals(1, result.getGuest().getStatistics().getGamesPlayed());
		assertEquals(1, result.getGuest().getStatistics().getGamesWon());
		assertEquals(0, result.getGuest().getStatistics().getGamesLost());
	}
	
	
	@Test
	public void shouldUpdateProperBothPlayersWhenDrawn() {
		// given
		given(userDAO.findByEmail(Mockito.any(String.class))).willReturn(createTestUser1());
		MatchTO match = createDrawnMatch();
		// when
		MatchTO result = matchStatsUpdateService.update(match);
		// then
		assertEquals(1, result.getHost().getStatistics().getGamesPlayed());
		assertEquals(1, result.getGuest().getStatistics().getGamesPlayed());
		
		assertEquals(1, result.getHost().getStatistics().getGamesDrawn());
		assertEquals(1, result.getGuest().getStatistics().getGamesDrawn());
		
		assertEquals(0, result.getHost().getStatistics().getGamesWon());
		assertEquals(0, result.getGuest().getStatistics().getGamesWon());
		
		assertEquals(0, result.getHost().getStatistics().getGamesLost());
		assertEquals(0, result.getGuest().getStatistics().getGamesLost());
	}
	
	private MatchTO createHostWonMatch() {
		UserProfileTO host = createTestUser1();
		UserProfileTO guest = createTestUser2(); 
		
		MatchStatus result = MatchStatus.WON;
		MatchTO match = new MatchTO();
		match.setGuest(guest);
		match.setHost(host);
		match.setResult(result);
		return match;
	}
	
	private MatchTO createGuestWonMatch() {
		UserProfileTO host = createTestUser1();
		UserProfileTO guest = createTestUser2(); 
		
		MatchStatus result = MatchStatus.LOSE;
		MatchTO match = new MatchTO();
		match.setGuest(guest);
		match.setHost(host);
		match.setResult(result);
		return match;
	}
	
	private MatchTO createDrawnMatch() {
		UserProfileTO host = createTestUser1();
		UserProfileTO guest = createTestUser2(); 
		
		MatchStatus result = MatchStatus.DRAWN;
		MatchTO match = new MatchTO();
		match.setGuest(guest);
		match.setHost(host);
		match.setResult(result);
		return match;
	}
	
	private UserStatisticsTO createTestStats() {
		UserStatisticsTO stats = new UserStatisticsTO();
		stats.setGamesDrawn(0);
		stats.setGamesLost(0);
		stats.setGamesPlayed(0);
		stats.setGamesWon(0);
		stats.setId(1);
		stats.setLevel(0);
		stats.setPoints(0);
		stats.setPosition(0);
		return stats;
	}
	
	private UserProfileTO createTestUser1() {
		UserProfileTO user = new UserProfileTO();
		user.setEmail("steve@bar.com");
		user.setId(1);
		user.setLifeMotto("Do what you had to");
		user.setLogin("stallone");
		user.setName("Sylvester");
		user.setSurname("Stallone");
		user.setPassword("steve123");
		user.setAboutMe("Human being");
		user.setStatistics(createTestStats());
		return user;
	}
	
	private UserProfileTO createTestUser2() {
		UserProfileTO user = new UserProfileTO();
		user.setAboutMe("yoopikay yeey");
		user.setEmail("john@bar.com");
		user.setId(2);
		user.setLifeMotto("Human being");
		user.setLogin("mcclane");
		user.setName("John");
		user.setPassword("A");
		user.setStatistics(createTestStats());
		user.setSurname("McClane");
		return user;
	}
}
