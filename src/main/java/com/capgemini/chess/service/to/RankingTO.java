package com.capgemini.chess.service.to;

import java.util.List;

public class RankingTO {
	
	private List<UserProfileTO> ranking;
	private int position;
	
	public List<UserProfileTO> getRanking() {
		return ranking;
	}
	public void setRanking(List<UserProfileTO> ranking) {
		this.ranking = ranking;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	
	
}
