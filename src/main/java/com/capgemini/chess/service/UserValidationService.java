package com.capgemini.chess.service;

import com.capgemini.chess.exception.UserValidationException;
import com.capgemini.chess.service.to.UserProfileTO;

public interface UserValidationService {
	
	public void validateUser(UserProfileTO user) throws UserValidationException;
}
