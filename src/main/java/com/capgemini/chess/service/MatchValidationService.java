package com.capgemini.chess.service;

import com.capgemini.chess.exception.MatchValidationException;
import com.capgemini.chess.service.to.MatchTO;

public interface MatchValidationService {
	public MatchTO validate(MatchTO to) throws MatchValidationException;
}
